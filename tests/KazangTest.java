import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for the application.
 */
public class KazangTest {
    //set to false if you do not want to delete the files after unit tests
    private boolean cleanup_after_tests = true;

    private String user_directory = System.getProperty("user.dir");
    private String input_directory = user_directory + "\\tests\\input\\";
    private String output_directory = user_directory + "\\tests\\expected\\";

    //cleanup the generated files in the input folder once a unit test is complete
    private void cleanUp(String file) {
        if (cleanup_after_tests) {
            File to_delete = new File(file);
            if (to_delete.delete()) {
                System.out.println("File deleted successfully");
            } else {
                System.out.println("Failed to delete the file");
            }
        }
    }

    //used to compare the output and expected files
    private String getUTF8(String url) {
        byte[] file = new byte[0];
        try {
            file = Files.readAllBytes(Paths.get(url));
        } catch (IOException e) {
            System.out.println("File " + url + " not found.");
        }
        return new String(file, StandardCharsets.UTF_8);
    }

    //tests against the output file provided. Had to modify the provided file as the MTN 5.00 was truncated to 5 in the example.
    // I believe it should be 5.00 as with the other examples. The trailing column space was also truncated in the example on the last column.
    // This has been modified to ensure the columns have a fixed size provided in the file header.
    @Test
    public void providedOutput() throws Exception {
        new Kazang(input_directory + "KazangBulkVouchers_Demo.txt");
        String input = getUTF8(input_directory + "KazangBulkVouchers_Demo_result.txt");
        String expected = getUTF8(output_directory + "KazangBulkVouchers_Demo_result.txt");
        cleanUp(input_directory + "KazangBulkVouchers_Demo_result.txt");
        assertEquals("The content in the files should match.", input, expected);
    }

    //Check to see if the output is modified correctly when the column width in the header is changed
    @Test
    public void columnWidth() throws Exception {
        new Kazang(input_directory + "columnWidth.txt");
        String input = getUTF8(input_directory + "columnWidth_result.txt");
        String expected = getUTF8(output_directory + "columnWidth_result.txt");
        cleanUp(input_directory + "columnWidth_result.txt");
        assertEquals("The content in the files should match.", input, expected);
    }

    //Check to see if the output is modified correctly when the column spacing in the header is changed
    @Test
    public void columnSpacing() throws Exception {
        new Kazang(input_directory + "columnSpacing.txt");
        String input = getUTF8(input_directory + "columnSpacing_result.txt");
        String expected = getUTF8(output_directory + "columnSpacing_result.txt");
        cleanUp(input_directory + "columnSpacing_result.txt");
        assertEquals("The content in the files should match.", input, expected);
    }

    //Check to see if the output is modified correctly when the left margin in the header is changed
    @Test
    public void leftMargin() throws Exception {
        new Kazang(input_directory + "leftMargin.txt");
        String input = getUTF8(input_directory + "leftMargin_result.txt");
        String expected = getUTF8(output_directory + "leftMargin_result.txt");
        cleanUp(input_directory + "leftMargin_result.txt");
        assertEquals("The content in the files should match.", input, expected);
    }

    //Check to see if the output is modified correctly when the row spacing in the header is changed
    @Test
    public void rowSpacing() throws Exception {
        new Kazang(input_directory + "rowSpacing.txt");
        String input = getUTF8(input_directory + "rowSpacing_result.txt");
        String expected = getUTF8(output_directory + "rowSpacing_result.txt");
        cleanUp(input_directory + "rowSpacing_result.txt");
        assertEquals("The content in the files should match.", input, expected);
    }

    //Check to see if the output is modified correctly when the line items in the header is increased
    @Test
    public void lineItemMore() throws Exception {
        new Kazang(input_directory + "lineItem.txt");
        String input = getUTF8(input_directory + "lineItem_result.txt");
        String expected = getUTF8(output_directory + "lineItem_result.txt");
        cleanUp(input_directory + "lineItem_result.txt");
        assertEquals("The content in the files should match.", input, expected);
    }

    //Check to see if the output is modified correctly when the line items in the header is decreased
    @Test
    public void lineItemLess() throws Exception {
        new Kazang(input_directory + "lineItem2.txt");
        String input = getUTF8(input_directory + "lineItem2_result.txt");
        String expected = getUTF8(output_directory + "lineItem2_result.txt");
        cleanUp(input_directory + "lineItem2_result.txt");
        assertEquals("The content in the files should match.", input, expected);
    }

    //Check to see if the output is modified correctly when the voucher fields in the header is modified
    @Test
    public void voucherFields() throws Exception {
        new Kazang(input_directory + "voucherFields.txt");
        String input = getUTF8(input_directory + "voucherFields_result.txt");
        String expected = getUTF8(output_directory + "voucherFields_result.txt");
        cleanUp(input_directory + "voucherFields_result.txt");
        assertEquals("The content in the files should match.", input, expected);
    }

    //Check to see if the output is modified correctly when the voucher fields in the header is in a different order
    @Test
    public void voucherFieldsSwitch() throws Exception {
        new Kazang(input_directory + "voucherFields2.txt");
        String input = getUTF8(input_directory + "voucherFields2_result.txt");
        String expected = getUTF8(output_directory + "voucherFields2_result.txt");
        cleanUp(input_directory + "voucherFields2_result.txt");
        assertEquals("The content in the files should match.", input, expected);
    }

    //Check to see if an error is thrown if validation fails
    @Test(expected = CustomException.class)
    public void validation() throws Exception {
        new Kazang(input_directory + "validation.txt");
    }

    //Check to see if an error is thrown if there are no voucher fields in the header
    @Test(expected = CustomException.class)
    public void noVoucherFields() throws Exception {
        new Kazang(input_directory + "noVoucherFields.txt");
    }

    //Check to see if an error is thrown if there is an invalid entry in the body of the voucher
    @Test(expected = CustomException.class)
    public void unmatchedVoucherFormat() throws Exception {
        new Kazang(input_directory + "unmatchedVoucherFormat.txt");
    }
}