/**
 * Stores the voucher body data
 */

public class VoucherEntry {
    private String provider;
    private String description;
    private String pin;
    private String serial_number;
    private String expiry_date;

    VoucherEntry(String provider, String description, String pin, String serial_number, String expiry_date) {
        this.provider = provider;
        this.description = description;
        this.pin = pin;
        this.serial_number = serial_number;
        this.expiry_date = expiry_date;
    }

    String getProvider() {
        return provider;
    }

    public String getDescription() {
        return description;
    }

    public String getPin() {
        return pin;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

}
