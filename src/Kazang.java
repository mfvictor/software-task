import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class reads data from a file, creates the appropriate data structure and print the data to the file.
 */

public class Kazang {

    private final static Pattern HEADER_PATTERN = Pattern.compile("^(?<field>[a-z_]+):(?<value>.+?)$");
    private final static String PROVIDER_DESC_REGEX = "(?<provider>[A-z-]+)\\\\s(?<description>\\\\d+.\\\\d*)";
    private final static String PIN_REGEX = "(?<pin>\\\\d+)";
    private final static String SERIAL_REGEX = "(?<serial>\\\\d+)";
    private final static String EXPIRY_REGEX = "(?<expiry>(?:19|20)\\\\d\\\\d[-.](?:0[1-9]|1[012])[-.](?:0[1-9]|[12][0-9]|3[01]))";
    private String FILE = "";

    //upon creating a new instance read, process and write data
    Kazang(String filename) {
        try {
            this.FILE = filename;
            Voucher data = readInputFile();
            writeOutputFile(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Kazang(args[0]);
    }

    //read from a file and create voucher structure
    private Voucher readInputFile() throws IOException {
        Voucher voucher = null;
        String line;
        try {
            BufferedReader br = new BufferedReader(new FileReader(FILE));
            boolean read_header = true;
            HashMap<String, String> fieldMap = new HashMap<>();
            ArrayList<String> line_items = new ArrayList<>();
            HashMap<String, Integer> voucher_summaries = new HashMap<>();
            String generated_regex;

            while ((line = br.readLine()) != null) {
                Matcher header_matcher = HEADER_PATTERN.matcher(line);
                Matcher body_matcher;
                if (read_header) {
                    if (header_matcher.find()) {
                        String field = header_matcher.group("field");
                        String value = header_matcher.group("value");


                        switch (field) {
                            case "line_item":
                                line_items.add(value);
                                break;
                            case "voucher_summary":
                                String[] temp_summaries = value.split(",");
                                voucher_summaries.put(temp_summaries[0], Integer.parseInt(temp_summaries[1]));
                                break;
                            default:
                                fieldMap.put(field, value);
                                break;
                        }
                    } else {
                        voucher = new Voucher(fieldMap, line_items, voucher_summaries);
                        read_header = false;
                    }
                }
                if (!read_header) {
                    String[] voucher_fields;
                    voucher_fields = fieldMap.getOrDefault("voucher_fields", "").split(",");
                    generated_regex = fieldMap.getOrDefault("voucher_fields", "")
                            .replaceAll("description", PROVIDER_DESC_REGEX)
                            .replaceAll("pin", PIN_REGEX)
                            .replaceAll("serial_number", SERIAL_REGEX)
                            .replaceAll("expiry_date", EXPIRY_REGEX);
                    if (!generated_regex.equals("")) {
                        Pattern BODY_PATTERN = Pattern.compile(generated_regex);
                        body_matcher = BODY_PATTERN.matcher(line);
                    } else {
                        throw new CustomException("The voucher_fields entry in the header of the file seems to be missing. Processing will now exit.");
                    }

                    if (body_matcher.find()) {
                        String provider = "";
                        String description = "";
                        String pin = "";
                        String serial = "";
                        String expiry = "";

                        if (Arrays.stream(voucher_fields).anyMatch("description"::equals)) {
                            provider = body_matcher.group("provider");
                            description = body_matcher.group("description");
                        }
                        if (Arrays.stream(voucher_fields).anyMatch("pin"::equals)) {
                            pin = body_matcher.group("pin");
                        }
                        if (Arrays.stream(voucher_fields).anyMatch("serial_number"::equals)) {
                            serial = body_matcher.group("serial");
                        }
                        if (Arrays.stream(voucher_fields).anyMatch("expiry_date"::equals)) {
                            expiry = body_matcher.group("expiry");
                        }
                        voucher.add_voucher_entry(new VoucherEntry(provider, description, pin, serial, expiry));
                    } else {
                        throw new CustomException("Entry [" + line + "] does not match the format " + Arrays.toString(voucher_fields) + ". Processing will continue and skip this entry.");
                    }
                }
            }
            br.close();
        } catch (FileNotFoundException e) {
            throw new CustomException("The file " + FILE + " was not found.");
        }

        return voucher;
    }

    //write to an output file
    private void writeOutputFile(Voucher data) throws IOException {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(FILE.replaceAll("\\.", "_result.")), "utf-8"))) {
            writer.write(data.printVoucher());
        } catch (Exception e) {
            throw new CustomException("An Error has occurred writing to the output file.");
        }
    }
}
