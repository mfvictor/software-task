import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Voucher data structure that hold, validates and formats output.
 */

class Voucher {
    private String download_datetime;
    private String order_datetime;
    private String layout_name;
    private int columns;
    private int column_width;
    private int column_spacing;
    private int left_margin;
    private int row_spacing;
    private ArrayList<String> line_item;
    private HashMap<String, Integer> voucher_summary;
    private String voucher_fields;

    private LinkedList<VoucherEntry> voucher_entries = new LinkedList<>();

    //sets the header data accumulated during read
    Voucher(HashMap<String, String> fieldMap, ArrayList<String> line_item, HashMap<String, Integer> voucher_summary) {
        this.download_datetime = fieldMap.getOrDefault("download_datetime", "");
        this.order_datetime = fieldMap.getOrDefault("order_datetime", "");
        this.layout_name = fieldMap.getOrDefault("layout_name", "");
        this.columns = Integer.parseInt(fieldMap.getOrDefault("columns", "0"));
        this.column_width = Integer.parseInt(fieldMap.getOrDefault("column_width", "0"));
        this.column_spacing = Integer.parseInt(fieldMap.getOrDefault("column_spacing", "0"));
        this.left_margin = Integer.parseInt(fieldMap.getOrDefault("left_margin", "0"));
        this.row_spacing = Integer.parseInt(fieldMap.getOrDefault("row_spacing", "0"));
        this.line_item = line_item;
        this.voucher_summary = voucher_summary;
        this.voucher_fields = fieldMap.getOrDefault("voucher_fields", "");
    }

    //adds a voucher entry
    void add_voucher_entry(VoucherEntry voucher_entry) {
        int temp = voucher_summary.getOrDefault(voucher_entry.getProvider() + " " + voucher_entry.getDescription(), 0);
        if (temp > 0) {
            this.voucher_entries.add(voucher_entry);
            voucher_summary.put(voucher_entry.getProvider() + " " + voucher_entry.getDescription(), temp - 1);
        } else {
            throw new CustomException("An error has occured in voucher " + voucher_entry.getPin() + ". It does not match the summary data in the header of the file.");
        }
    }

    //provides the print format for the data
    String printVoucher() {
        StringBuilder formatted_data = new StringBuilder();
        String format_spacing = "";
        StringBuilder left_padding = new StringBuilder();
        ArrayList<VoucherEntry> entry = new ArrayList<>();
        VoucherEntry[] vent = new VoucherEntry[columns];

        for (int m = 0; m < left_margin; m++) {
            left_padding.append(" ");
        }

        for (int i = 0; i < voucher_entries.size(); i++) {
            String[] pin = new String[columns];
            String[] provider_desc = new String[columns];
            String[] serial = new String[columns];
            String[] expiry = new String[columns];

            entry.add(voucher_entries.get(i));
            format_spacing += "%-" + (column_width + column_spacing) + "s";//column spacing
            if ((entry.size() == columns) || i == voucher_entries.size() - 1) {
                for (int j = 0; j < entry.size(); j++) {
                    pin[j] = entry.get(j).getPin();
                    provider_desc[j] = entry.get(j).getProvider() + " " + entry.get(j).getDescription();
                    serial[j] = entry.get(j).getSerial_number();
                    expiry[j] = entry.get(j).getExpiry_date();
                }

                for (String x : line_item) {
                    switch (x) {
                        case "pin":
                            formatted_data.append(left_padding.toString()).append(String.format(format_spacing, (Object[]) pin));
                            formatted_data.append("\n");
                            break;
                        case "description":
                            formatted_data.append(left_padding.toString()).append(String.format(format_spacing, (Object[]) provider_desc));
                            formatted_data.append("\n");
                            break;
                        case "serial_number":
                            formatted_data.append(left_padding.toString()).append(String.format(format_spacing, (Object[]) serial));
                            formatted_data.append("\n");
                            break;
                        case "expiry_date":
                            formatted_data.append(left_padding.toString()).append(String.format(format_spacing, (Object[]) expiry));
                            formatted_data.append("\n");
                            break;
                        case "empty":
                            formatted_data.append("\n");
                            break;
                        default:
                            break;
                    }
                }

                for (int k = 0; k < row_spacing; k++) {
                    formatted_data.append("\n");
                }

                format_spacing = "";
                entry = new ArrayList<>();
            }
        }
        return formatted_data.toString();
    }

}

