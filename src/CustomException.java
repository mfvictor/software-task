/**
 * An unchecked custom exception used within the application.
 */

class CustomException extends RuntimeException {
    CustomException(String s) {
        super(s);
    }
}
